/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Graphics;
import javax.swing.tree.DefaultTreeModel;
import ufps.util.colecciones_seed.ArbolBinario;
import ufps.util.colecciones_seed.ArbolDigital;
import ufps.util.colecciones_seed.Busqueda;

/**
 *
 * @author Javier
 */
public class Controlador {

    private ArbolDigital arbolDi;
    private Busqueda busqueda;

    public Controlador() {
        arbolDi = new ArbolDigital();
        busqueda = new Busqueda();
    }

    public ArbolBinario hacerArbol(String doc) {
        ArbolBinario arbol = arbolDi.crearArbol(doc);
        return arbol;
    }

    public DefaultTreeModel pintar() {
        return arbolDi.pintar();
    }
     
    public boolean Buscarpalabra(String palabra){
        busqueda = arbolDi.buscarPa(palabra);
        return busqueda.getCodigo().equals("");
    }
    
    public String getCodiBus(){
        return busqueda.getCodigo();
    }

    public int getCantBus(){
        return busqueda.getCantidad();
    }
    
    public String buscarPru(String palabra){
        return arbolDi.buscarPalabra(palabra);
    }
    
    public String txt() {
        return arbolDi.txt();
    }

    public ArbolDigital este() {
        return this.arbolDi;
    }

    public void graficos(Graphics g, Graphics g2) {
        arbolDi.graficos(g, g2);
    }
}
