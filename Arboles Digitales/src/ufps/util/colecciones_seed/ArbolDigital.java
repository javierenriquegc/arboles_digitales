/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author Javier
 */
public class ArbolDigital<T> {

    static private Codificacion[] codificacion;
    private ArbolBinario arbol;
    private int espacio = 40;

    public ArbolDigital() {
        arbol = new ArbolBinario(".");
        //Se agregaron ".","z" y " "
        codificacion = new Codificacion[32];
        codificacion[0] = new Codificacion(" ", "00000");
        codificacion[1] = new Codificacion("a", "00001");
        codificacion[2] = new Codificacion("b", "00010");
        codificacion[3] = new Codificacion("c", "00011");
        codificacion[4] = new Codificacion("d", "00100");
        codificacion[5] = new Codificacion("e", "00101");
        codificacion[6] = new Codificacion("f", "00110");
        codificacion[7] = new Codificacion("g", "00111");
        codificacion[8] = new Codificacion("h", "01000");
        codificacion[9] = new Codificacion("i", "01001");
        codificacion[10] = new Codificacion("j", "01010");
        codificacion[11] = new Codificacion("k", "01011");
        codificacion[12] = new Codificacion("l", "01100");
        codificacion[13] = new Codificacion("m", "01101");
        codificacion[14] = new Codificacion("n", "01110");
        codificacion[15] = new Codificacion("o", "01111");
        codificacion[16] = new Codificacion("p", "10000");
        codificacion[17] = new Codificacion("q", "10001");
        codificacion[18] = new Codificacion("r", "10010");
        codificacion[19] = new Codificacion("s", "10011");
        codificacion[20] = new Codificacion("t", "10100");
        codificacion[21] = new Codificacion("u", "10101");
        codificacion[22] = new Codificacion("v", "10110");
        codificacion[23] = new Codificacion("w", "10111");
        codificacion[24] = new Codificacion("x", "11000");
        codificacion[25] = new Codificacion("y", "11001");
        codificacion[26] = new Codificacion(":", "11010");
        codificacion[27] = new Codificacion(";", "11011");
        codificacion[28] = new Codificacion(",", "11100");
        codificacion[29] = new Codificacion("-", "11101");
        codificacion[30] = new Codificacion(".", "11110");
        codificacion[31] = new Codificacion("z", "11111");
    }

    public ArbolBinario crearArbol(String doc) {
        arbol = new ArbolBinario(".");
        String[] docs = doc.split("");
        for (int i = 0; i < docs.length; i++) {
            for (int j = 0; j < codificacion.length; j++) {
                if (docs[i].equalsIgnoreCase(codificacion[j].getCaracter())) {
                    String[] bits = codificacion[j].getCod().split("");
                    insertar(bits, docs[i], "" + i);
                }
            }
        }

        return arbol;
    }

    public void insertar(String[] bits, String caracter, String posicion) {
        NodoBin<T> padre = arbol.getRaiz();
        //Buscar en las hojas si el caracter ya se encuentra registrado y solo agegar la posicion
        boolean existe = false;
        ListaCD<ArrayList<String>> it = arbol.getHojasA();
        if (it != null && !posicion.equals("0")) {
            int i = 0;
            while (i < it.getTamanio()) {
                //System.out.print(it.get(i).get(0)+ "\t");
                if (it.get(i).get(0).equalsIgnoreCase(caracter)) {
                    it.get(i).add(posicion);
                    existe = true;
                }
                i++;
            }
        }
//        int eqiz [] = {300,150,75,37,18,9}; 
        int eqiz[] = {390, 195, 97, 48, 24, 12};
        if (!existe) {
            //System.out.println("");
            for (int i = 0; i < bits.length; i++) {
                NodoBin<T> hijo = new NodoBin<>((T) bits[i]);
                if (bits[i].equals("0")) {
                    if (padre.getIzq() == null) {
                        padre.setIzq(hijo);
                        int pos = arbol.nivel1(hijo);
                        hijo.setPosition(padre.getPositionX() - eqiz[pos], padre.getPositionY() + espacio);
                        padre = hijo;
                    } else {
                        padre = padre.getIzq();
                    }
                } else {
                    if (padre.getDer() == null) {
                        padre.setDer(hijo);
                        int pos = arbol.nivel1(hijo);
                        hijo.setPosition(padre.getPositionX() + eqiz[pos], padre.getPositionY() + espacio);
                        padre = hijo;
                    } else {
                        padre = padre.getDer();
                    }
                }
            }
            ArrayList<String> carac = new ArrayList<>();
            carac.add(caracter);
            carac.add(posicion);
            NodoBin<ArrayList<String>> hoja = new NodoBin<>(carac);
            padre.setIzqA(hoja);

            padre.getIzq().setPosition(padre.getPositionX(), padre.getPositionY() + espacio);
        }
    }

    /*Metodo de busqueda usado en el programa*/
    public Busqueda buscarPa(String palabra) {
        String[] letras = palabra.split("");
        ArrayList<ArrayList<String>> filtrado = new ArrayList<ArrayList<String>>();
        ListaCD<ArrayList<String>> it = arbol.getHojasA();
        if (it != null) {
            for (int j = 0; j < letras.length; j++) {
                int i = 0;
                boolean existe = false;
                while (i < it.getTamanio()) {
                    if (it.get(i).get(0).equalsIgnoreCase(letras[j])) {
                        existe = true;
                        filtrado.add(it.get(i));
                    }
                    i++;
                }
                if (!existe) {
                    return new Busqueda();
                }
            }
            int cantidad = 0;
            for (int i = 1; i < filtrado.get(0).size(); i++) {
                if (compararBusqueda(filtrado, 0, 1, i)) {
                    cantidad++;
                }
            }
            if (cantidad > 0) {
                System.out.println("-------SI-------");
                System.out.println("Cantidad: " + cantidad);
                return new Busqueda(generarCodigo(filtrado), cantidad);
            } else {
                System.out.println("-------NO-------");
                return new Busqueda();
            }
        }
        return new Busqueda();
    }

    private boolean compararBusqueda(ArrayList<ArrayList<String>> fil, int posa, int posb, int ini) {
        if (posa < fil.size() && posb < fil.size()) {
            ArrayList<String> a = fil.get(posa);
            ArrayList<String> b = fil.get(posb);
            int i = ini;
            for (int j = 1; j < b.size(); j++) {
                int posA = Integer.parseInt(a.get(i)) + 1;
                int posB = Integer.parseInt(b.get(j));
                if (posA == posB) {
                    return compararBusqueda(fil, posa + 1, posb + 1, j);
                }
            }
            return false;
        } else {
            return true;
        }
    }

    private String generarCodigo(ArrayList<ArrayList<String>> fil) {
        String codigo = "";
        for (int i = 0; i < fil.size(); i++) {
            NodoBin nod = arbol.buscar(fil.get(i));
            String[] parCodi = new String[arbol.getAltura() - 2];
            for (int k = 0; k < arbol.getAltura() - 2; k++) {
                parCodi[k] = arbol.getPadre1(nod).getInfo().toString();
                nod = arbol.getPadre1(nod);
            }
            for (int k = parCodi.length - 1; k >= 0; k--) {
                codigo += parCodi[k];
            }
        }
        System.out.println(codigo);
        return codigo;
    }

    public DefaultTreeModel pintar() {
        return arbol.pintar();
    }

    public String txt() {
        return arbol.texto();
    }

    public void graficos(Graphics g, Graphics g2) {
        arbol.pintarEnGraphics(g, g2);
    }

    /*Buscar Palabra metodo 1.0 este es el primer metodo que hice para la busqueda de palabras, 
    decidi replantear el metodo ya que este aunque funciona no me parece optimo.*/
    public String buscarPalabra(String palabra) {
        String[] letras = palabra.split("");
        System.out.println("====????");
        String codigo = "";

        ListaCD<ArrayList<String>> it = arbol.getHojasA();
        if (it != null) {
            boolean consecutivas = false;
            String pos = "";
            boolean salir = false;
            int posi = 0;
            for (int j = 0; j < letras.length; j++) {
                System.out.println("Letra Actual: " + letras[j]);
                int i = 0;
                while (i < it.getTamanio()) {
                    System.out.println("Hoja " + it.get(i).get(0) + " - letra: " + letras[j]);
                    if (it.get(i).get(0).equalsIgnoreCase(letras[j])) {
                        if (!consecutivas) {
                            System.out.println("Consecutiva? No");
                            posi++;
                            System.out.println("Pos de letra en Hoja: " + posi);
                            if (posi >= it.get(i).size()) {
                                salir = true;
                                break;
                            }
                            pos = it.get(i).get(posi);
                            consecutivas = true;

                            NodoBin nod = arbol.buscar(it.get(i));
                            String[] parCodi = new String[arbol.getAltura() - 2];
                            for (int k = 0; k < arbol.getAltura() - 2; k++) {
                                parCodi[k] = arbol.getPadre1(nod).getInfo().toString();
                                nod = arbol.getPadre1(nod);
                            }
                            for (int k = parCodi.length - 1; k >= 0; k--) {
                                codigo += parCodi[k];
                            }

                            break;
                        } else if (consecutivas) {
                            System.out.println("Consecutiva? Si");
                            boolean si = false;
                            for (int u = 1; u < it.get(i).size(); u++) {
                                int posA = Integer.parseInt(pos) + 1;
                                int posB = Integer.parseInt(it.get(i).get(u));
                                System.out.println("Pos de letra en Hoja: " + u);

                                System.out.println("posB " + posB + " - PosA " + posA);
                                if (posA == posB) {
                                    pos = "" + posA;
                                    si = true;
                                    NodoBin nod = arbol.buscar(it.get(i));
                                    String[] parCodi = new String[arbol.getAltura() - 2];
                                    for (int k = 0; k < arbol.getAltura() - 2; k++) {
                                        parCodi[k] = arbol.getPadre1(nod).getInfo().toString();
                                        nod = arbol.getPadre1(nod);
                                    }
                                    for (int k = parCodi.length - 1; k >= 0; k--) {
                                        codigo += parCodi[k];
                                    }
                                    break;
                                } else {
                                    si = false;
                                }
                            }
                            if (si) {
                                break;
                            } else {
                                i = -1;
                                j = 0;
                                codigo = "";
                                consecutivas = false;
                            }
                        }
                    } else if (i + 1 >= it.getTamanio()) {
                        consecutivas = false;
                        salir = true;
                    }
                    i++;
                }
                if (salir) {
                    break;
                }

            }
            if (consecutivas) {
                System.out.println("-------SI-------");
                System.out.println(codigo);
                return codigo;
            } else {
                System.out.println("-------NO-------");
                return "";
            }
        }

        return "";
    }
}
