/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author Javier
 */
public class Busqueda {
    
    private String codigo = "";
    private int cantidad = 0;
    
    public Busqueda(){
    }
    
    public Busqueda(String codigo, int cantidad){
        this.codigo = codigo;
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public int getCantidad() {
        return cantidad;
    }
}
