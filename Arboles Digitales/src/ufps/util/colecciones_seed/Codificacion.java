/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author Javier
 */
public class Codificacion {
    private String caracter;
    private String cod;
    
    public Codificacion(String caracter, String cod){
        this.caracter = caracter;
        this.cod = cod;
    }

    public String getCaracter() {
        return caracter;
    }

    public void setCaracter(String letra) {
        this.caracter = letra;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }
    
    
}
