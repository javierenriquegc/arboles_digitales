/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author edye
 */
public class NodoAD<T> {
    
     private T info;
   
    private NodoAD<T> hijo;        
    
   
    private NodoAD<T> hermano;     
    
    

    public NodoAD(T info){
        this.info= info ;
        this.hijo=null;
        this.hermano=null;
    }
    
    public NodoAD(T info, NodoAD<T> hi, NodoAD<T> he){
        this.info = info;
        this.hijo = hi;
        this.hermano = he;
    }
    
  
    public T getInfo() {
        return info;
    }

   
    public void setInfo(T info) {
        this.info = info;
    }
  
    public NodoAD<T> getHijo() {
        return hijo;
    }

    public void setHijo(NodoAD<T> hijo) {
        this.hijo = hijo;
    }

   
    public NodoAD<T> getHermano() {
        return hermano;
    }

   
    public void setHermano(NodoAD<T> hermano) {
        this.hermano = hermano;
    }

}
